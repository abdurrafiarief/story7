function changeTheme(){
    var body = document.getElementById("body");
    var currentClass = body.className;
    if (currentClass == "orange-theme"){
        body.className = "sky-theme";
        document.getElementById("card1").className = "accordian-skytitle";
        document.getElementById("card2").className = "accordian-skytitle";
        document.getElementById("card3").className = "accordian-skytitle";

        document.getElementById("cardcontent1").className = "accordian-skytext";
        document.getElementById("cardcontent2").className = "accordian-skytext";
        document.getElementById("cardcontent3").className = "accordian-skytext";
        
        
    }
    else{
        body.className = "orange-theme";
        document.getElementById("card1").className = "accordian-orangetitle";
        document.getElementById("card2").className = "accordian-orangetitle";
        document.getElementById("card3").className = "accordian-orangetitle";

        document.getElementById("cardcontent1").className = "accordian-orangetext";
        document.getElementById("cardcontent2").className = "accordian-orangetext";
        document.getElementById("cardcontent3").className = "accordian-orangetext";
    }

}

$(document).ready(function() {
    
    $('.accordian-orangetext').hide();

    if($('body').className == "orange-theme"){
    

    
    $('.toggle').click(function(e) {
        e.preventDefault();
    
      var $this = $(this);
    
      if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
      } else {
          $this.parent().parent().find('li .accordian-orangetext').removeClass('show');
          $this.parent().parent().find('li .accordian-orangetext').slideUp(350);
          $this.next().toggleClass('show');
          $this.next().slideToggle(350);
      }
    });
    }
    else{
    
    $('.toggle').click(function(e) {
        e.preventDefault();
    
      var $this = $(this);
    
      if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
      } else {
          $this.parent().parent().find('li .accordian-skytext').removeClass('show');
          $this.parent().parent().find('li .accordian-skytext').slideUp(350);
          $this.next().toggleClass('show');
          $this.next().slideToggle(350);
      }
    });
    }

  });