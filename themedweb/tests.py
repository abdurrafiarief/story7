from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from .views import landing
from .urls import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Create your tests here.
class web_page_tests(TestCase):
    def test_landing_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_landing_func_works(self):
        reqeust = HttpRequest()
        response = landing(reqeust)
        html_response = response.content.decode('utf8')
        self.assertIn('Abdurrafi Arief', html_response)

class story7_functional_test(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')
    
    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_input_todo(self):
        self.selenium.get(self.live_server_url + '/')

        self.selenium.implicitly_wait(10)

        button = self.selenium.find_element_by_name("theme-button")
        self.selenium.implicitly_wait(10)
        button.click()
        self.selenium.implicitly_wait(10)
        self.assertIn("sky-theme", self.selenium.page_source)

